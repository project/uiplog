<?php

/**
 * @file
 * Install, update and uninstall functions for the uiplog module.
 *
 */

/**
 * Implements hook_schema().
 */
function uiplog_schema() {
  $schema['uiplog'] = array(
    'description' => 'Logs users IP to db table on user login.',
    'fields' => array(
      'lid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique ID.',
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Maps to uid in user table.',
      ),
      'ip' => array(
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
        'description' => 'IP address of logged in users.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'description' => 'Time of login.',
      ),
    ),
    'indexes' => array(
      'uid' => array('uid'),
      'ip' => array('ip'),
    ),
    'primary key' => array('lid'),
  );
  return $schema;
}

/**
 * Implements hook_uninstall()
 */
function uiplog_uninstall() {
  views_invalidate_cache();
}

/**
 * Add default value for uiplog_user_delete_log to config.
 */
function uiplog_update_8101() {
  Drupal::configFactory()->getEditable('uiplog.settings')
    ->set('uiplog_user_delete_log', 1)
    ->save();
}